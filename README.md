# README #
## Bitbucket Pipelines Deploy-to-Azure Extension ##
This Visual Studio Code extension allows you to perform the following:

* Add a Bitbucket Pipelines configuration file, along with a supporting bash script and a documentation page, that allows you to deploy in a CI/CD fashion to Azure App Service.
* Navigate to the Bitbucket Pipelines page for your Bitbucket repository to monitor the status of your CI/CD builds.
* Navigate to the currently open source file in Bitbucket.

### Installing the Extension Locally ###
To install the extension locally, follow one of the following 2 procedures:

1. copy the extension's folder to the following locations:

    * **`Windows`**: %USERPROFILE%\.vscode\extensions
    * **`Mac`**: ~/.vscode/extensions
    * **`Linux`**: ~/.vscode/extensions 

2. Install the .vsix file located at the root of this repo by running the following command:

    * `code bitbucket-pipelines-deploy-to-azure-0.0.1.vsix`

### For more information
* [Azure](http://azure.microsoft.com)
* [Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/bitbucket-builds-beta-792496469.html)
* [Bitbucket Pipelines yml Reference](https://confluence.atlassian.com/bitbucket/bitbucket-pipelines-yml-reference-824475665.html)
* [Deploy-to-Azure Plugin](https://marketplace.atlassian.com/plugins/deploy-to-azure/cloud/overview)
* [Azure Deployment Credentials](https://github.com/projectkudu/kudu/wiki/Deployment-credentials)
* [Deploy your app to Azure App Service](https://azure.microsoft.com/en-us/documentation/articles/web-sites-deploy/)

**Enjoy!**
