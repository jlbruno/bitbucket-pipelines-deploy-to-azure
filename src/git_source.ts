// TypeScriptified and slightly modified version of extension.js:
// https://github.com/ziyasal/vscode-open-in-github/blob/master/src/extension.js

// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
// git: parse-git-config (Source: https://github.com/jonschlinkert/parse-git-config)
// parse: github-url-from-git (Source: https://github.com/tj/node-github-url-from-git)
// gitRev: git-rev-2 (Source: https://github.com/yanatan16/git-rev)

import * as vscode from 'vscode';
import * as path from 'path';
import * as fs from 'fs';
import * as open from './open';
var git = require('parse-git-config');
var parse = require('github-url-from-git');
var gitRev = require('git-rev-2');

export function getBitbucketUrl(pipelinesUrlSuffix:string) {
    var cwd = vscode.workspace.rootPath;
    
    git({
        cwd: cwd, path: '.git/config'
    }, function (err, config) {
        if (err) {
            console.log('getBitbucketUrl: Error parsing git configuration: message: ' + err);
            vscode.window.showWarningMessage('Failed to read source control configuration.');
            return;
        }
        
        var rawUri, parseOpts, lineIndex = 0, parsedUri, branch, editor, selection
            , projectName, subdir, gitLink: string, scUrls;

        scUrls = {
            bitbucket: 'https://bitbucket.org'
        }

        if (!config['remote \"origin\"']){
            vscode.window.showWarningMessage('Current project is not under source control, or source control is not configured.');
            return;
        }
        
        rawUri = config['remote \"origin\"'].url;
        parseOpts = {
            extraBaseUrls: ['bitbucket.org']
        }

        rawUri = rawUri.replace('bitbucket.org:', 'bitbucket.org/')

        parsedUri = parse(rawUri, parseOpts);
        if (!parsedUri) {
            parsedUri = rawUri;
        }
        
        let pipelineUrl:string = parsedUri + pipelinesUrlSuffix;
        vscode.window.showInformationMessage('Navigated to Pipelines.');
        open.open(pipelineUrl, '', null);
    });
}

export function openSourceInBitbucket() {
    var cwd = vscode.workspace.rootPath;

    git({
        cwd: cwd, path: '.git/config'
    }, function (err, config) {
        if (err) {
            console.log('openSourceInBitbucket: Error parsing git configuration: message: ' + err);
            vscode.window.showWarningMessage('Failed to read source control configuration.');
            return;
        }

        var rawUri, parseOpts, lineIndex = 0, parsedUri, branch, editor, selection
            , projectName, subdir, gitLink: string, scUrls;

        scUrls = {
            bitbucket: 'https://bitbucket.org'
        }

        if (!config['remote \"origin\"']){
            vscode.window.showWarningMessage('Current file is not under source control, or source control is not configured.');
            return;
        }
        
        rawUri = config['remote \"origin\"'].url;
        parseOpts = {
            extraBaseUrls: ['bitbucket.org']
        }

        rawUri = rawUri.replace('bitbucket.org:', 'bitbucket.org/')

        parsedUri = parse(rawUri, parseOpts);
        if (!parsedUri) {
            parsedUri = rawUri;
        }

        gitRev.branch(cwd, function (branchErr, branch) {
            if (branchErr || !branch)
                branch = 'master';
            editor = vscode.window.activeTextEditor;
            if (editor) {
                selection = editor.selection;

                lineIndex = selection.active.line + 1;
                projectName = parsedUri.substring(parsedUri.lastIndexOf("/") + 1, parsedUri.length);

                subdir = editor.document.uri.fsPath.substring(vscode.workspace.rootPath.length).replace(/\"/g, "");

                gitLink = parsedUri + "/src/" + branch + subdir + "#cl-" + lineIndex;

                if (parsedUri.startsWith(scUrls.bitbucket)) {
                    gitLink = parsedUri + "/src/" + branch + subdir + "#cl-" + lineIndex;
                } else {
                    vscode.window.showWarningMessage('Only Bitbucket is supported.');
                }
            } else {
                gitLink = gitLink = parsedUri + "/tree/" + branch;
            }

            if (gitLink)
                open.open(gitLink, '', null);
        });
    });
}
